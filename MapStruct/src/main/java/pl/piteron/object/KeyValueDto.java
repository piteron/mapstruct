package pl.piteron.object;

public class KeyValueDto {
  private String key;
  private String value;
  private String anotherField;

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public String getAnotherField() {
    return anotherField;
  }

  public void setAnotherField(String anotherField) {
    this.anotherField = anotherField;
  }
}
