package pl.piteron.mapper;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.mapstruct.Mapper;

import pl.piteron.object.KeyValue;

@Mapper
public abstract class MapListMapper {

  public Map<String, String> toMap(List<KeyValue> list) {
    return list.stream().collect(Collectors.toMap(KeyValue::getKey, KeyValue::getValue));
  }
}
