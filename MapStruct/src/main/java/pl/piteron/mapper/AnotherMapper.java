package pl.piteron.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import pl.piteron.object.KeyValue;
import pl.piteron.object.KeyValueDto;

@Mapper
public interface AnotherMapper {
  
  @Mapping(target = "anotherField", ignore=true)
  KeyValueDto map(KeyValue keyValue);
}
