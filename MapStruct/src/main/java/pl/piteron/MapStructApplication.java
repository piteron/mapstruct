package pl.piteron;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import pl.piteron.mapper.AnotherMapper;
import pl.piteron.mapper.AnotherSuperMapper;
import pl.piteron.mapper.MapListMapper;
import pl.piteron.object.KeyValue;
import pl.piteron.object.KeyValueDto;

@Configuration
@ComponentScan(basePackages = "pl.piteron.mapper")
@SpringBootApplication
public class MapStructApplication {

  public static void main(String[] args) {
    ConfigurableApplicationContext context = SpringApplication.run(MapStructApplication.class, args);
    Arrays.stream(context.getBeanDefinitionNames()).forEach(System.out::println);
    MapListMapper mapper = context.getBean(MapListMapper.class);
    List<KeyValue> list = new ArrayList<>();
    KeyValue keyValue = new KeyValue();
    keyValue.setKey("whoaaKey");
    keyValue.setValue("whoaaValue");
    list.add(keyValue);
    Map<String, String> map = mapper.toMap(list);
    System.out.println(map);
    
    AnotherMapper anotherMapper = context.getBean(AnotherMapper.class);
    KeyValueDto keyValueDto = anotherMapper.map(keyValue);
    System.out.println(keyValueDto);
    
    AnotherSuperMapper anotherSuperMapper = context.getBean(AnotherSuperMapper.class);
  }
}
